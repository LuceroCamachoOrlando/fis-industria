# MAPA1 - Industria 4.0
```plantuml
@startmindmap
*[#lightblue] Industria 4.0
**[#lightgreen] Es la completa digitalización de las cadenas de\nvalor a través de la integración de tecnologías de\nprocesamiento de datos, software inteligente y\nsensores; desde lso proveedores hasta los\nclientes, para así poder predecir, controlar,\nplanear, y producir, de forma inteligente, lo que\ngenera mayor valor a toda la cadena
***[#FFBBCC] Producción inteligente con decisión\nautónoma usando:
****[#20B2AA] Robótica
****[#20B2AA] Big Data
****[#20B2AA] CLoud Computing
****[#20B2AA] Sistema Autónomo
****[#20B2AA] IoT
****[#20B2AA] Inteligencia artificial
***[#FFBBCC] Cambios que generan
****[#20B2AA] Integración de TIC's\nen la industria de\nmanufactura y de servicios
***** Reducción de puestos de trabajo\nque requieran procesos repetitivos
***** Aparición de nuevas profesiones
****[#20B2AA] Transformación de las\n"empresas de\nmanufactura" en\n"empresas de TIC's"
***** Cada vez existe menos\ndiferenciación entre las\nindustrias
****[#20B2AA] Nuevos paradigmas\ny tecnologías
***** Aparición de negocios de\nplataforma, como FANG o BAT
***** El internet de las cosas abrirá\nla posibilidad de automatizar\npor completo los hogares
***** El recurso más valioso ya\nno es el petroleo, sino\nlos datos
****[#20B2AA] Nuevas culturas digitales
***** Dependencia de los seres\nhumanos a sus dispositivos\nmóviles
***** Los e-sports serán el segundo\ndeporte más visto en EU, tan\nsólo después de la NFL
***** Los nuevos empleos generados\npor las tecnologías están\ncreando completamente nuevas\nculturas de comportamiento\nentre los jóvenes
@endmindmap
```

# MAPA 2 - México y la Industria 4.0
```plantuml
@startmindmap
*[#lightblue] México y la Industria 4.0
**[#lightgreen] México está en un punto de inflexión\npor la cercanía a EU, México tiene la\noportunidad de crecer con las nuevas\ntecnologías
***[#FFBBCC] México está presente en la\ncreación de estas nuevas\ntecnologías, gracias a las\n startups, por ejemplo:
****[#20B2AA] SinLlaves, que ofrecen\nun futuro en el que no sea\nnecesario cargar con una\nllave física todo el tiempo
****[#20B2AA] Wearobot, desarrollaron un exoesqueleto\npara ayudar a recuperar la movilidad a las\npersonas, además de conectar al médico\ncon el paciente
***[#FFBBCC] La optimización de procesos sigue\nsiendo el referente para avanzar en\nla industria
****[#20B2AA] EL recorte de costos y optimización\ny creación de nuevas áreas de\nnegocio es lo que se espera por\nlos CEO's y CTO's
****[#20B2AA] Las empresas pequeñas quieren y están\ncomenzando a utilizar Inteligencia\nArtificial y Machine Learning para\nmejorar sus ritmos de trabajo y poder\nanalizar todo en tiempo real
****[#20B2AA] Los servicios en la nube son vitales\npara entrenar todas las nuevas tecnologías\nllegando a reemplazar la gestión de servidores\npropios
@endmindmap
```
